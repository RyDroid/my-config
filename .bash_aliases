# -*- mode: shell-script; -*-

# enable color support
if [ -x /usr/bin/dircolors ]
then
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some ls aliases
alias ll='ls -l'
alias la='ls -A'

# some grep aliases
alias texgrep='grep -r --include=*.tex'
alias pygrep='grep -r --include=*.py'
