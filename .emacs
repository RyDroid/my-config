(add-to-list 'load-path "~/.emacs.d/packages/")

(load-file "~/.emacs.d/ui.el")
(load-file "~/.emacs.d/languages.el")
(load-file "~/.emacs.d/vcs.el")

;; Temp files in ~/.saves
(setq backup-directory-alist `(("." . "~/.saves")))

;; No temp files (~)
;(setq make-backup-files nil)

;; Auto-save of session in current directory
;(desktop-save-mode 1)

;; http://www.emacswiki.org/emacs/DeleteSelectionMode
(delete-selection-mode 1)

(setq ring-bell-function 'ignore)

(prefer-coding-system 'utf-8)

;; Fichiers récents
(require 'recentf)
(setq recentf-max-saved-items 50)
(recentf-mode 1)

;; Interactively DO things
;(require 'ido)
;(setq ido-enable-flex-matching t) ;; fuzzy matching for sorting results
;(setq ido-everywhere t) ;; ido for find-files and buffers
;(ido-mode 1)

(put 'upcase-region   'disabled nil)
(put 'downcase-region 'disabled nil)

;; Package manager
;(require 'package)
;(add-to-list 'package-archives
;             '("melpa" . "https://melpa.org/packages/") t)
;(when (< emacs-major-version 24)
;  ;; For important compatibility libraries like cl-lib
;  (add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/")))
;(package-initialize)

;; Undo Tree http://www.dr-qubit.org/emacs.php#undo-tree-docs
;(require 'undo-tree)
;(global-undo-tree-mode)

;; http://editorconfig.org/
(require 'editorconfig)
(editorconfig-mode 1)

(load-library "iso-transl") ; dead-circumflex

(global-set-key "\C-ck" 'kill-whole-line) ; C-c k

(provide '.emacs)
;;; .emacs ends here
